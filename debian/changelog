mongo-tools (3.4.14-4) unstable; urgency=medium

  * Fix FTBFS with Go 1.11 (Closes: #920604)
  * Bump Standards-Version to 4.3.0; no changes needed

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Tue, 12 Feb 2019 14:48:02 +0200

mongo-tools (3.4.14-3) unstable; urgency=medium

  * Change maintainer address to tracker.d.o (Closes: #899614)
  * Bump Standards-Version to 4.1.4; no changes needed
  * d/rules: check DEB_BUILD_OPTIONS for presence of `nocheck'

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Wed, 20 Jun 2018 14:09:46 +0300

mongo-tools (3.4.14-2) unstable; urgency=medium

  * OpenSSL 1.1 support (Closes: #859222)
    - Backport and adjust spacemonkeygo's OpenSSL 1.1 support on top of
      MongoDB's forked code.
    - B-D on libssl-dev

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Thu, 29 Mar 2018 12:52:25 +0300

mongo-tools (3.4.14-1) unstable; urgency=medium

  [ Michael Stapelbert ]
  * Switch to XS-Go-Import-Path (Closes: #890059)

  [ Apollon Oikonomopoulos ]
  * New upstream version 3.4.14 (Closes: #891013)
    - New build dependencies:
      + golang-github-nsf-termbox-go-dev
      + golang-github-google-gopacket-dev
      + golang-github-golang-snappy-dev
      + libpcap-dev
    - Use vendorized 10gen/* and go-cache
  * d/watch: search for 3.4 stable releases
  * Bump Standards-Version to 4.1.3; no changes needed
  * Bump dh compat to 11; no changes needed
  * Switch Vcs-* URLs to salsa.d.o
  * Build and install new mongoreplay tool
    - Add mongoreplay(1) manpage
  * Do not run bsondump and mongoreplay tests; bsondump has no actual tests
    and mongoreplay requires a running mongod instance
  * Drop override_dh_golang
  * Fix typo in mongoimport.1
  * Sync d/copyright with upstream code

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Mon, 26 Mar 2018 14:33:56 +0300

mongo-tools (3.2.11-1) unstable; urgency=medium

  * New upstream release
  * Upload to unstable
  * B-D on libssl1.0-dev

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 16 Dec 2016 10:09:58 +0200

mongo-tools (3.2.9-2) experimental; urgency=medium

  * Add the missing epoch in Breaks & Replaces (Closes: #836861)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Thu, 08 Sep 2016 10:29:34 +0300

mongo-tools (3.2.9-1) experimental; urgency=medium

  * New upstream release.
  * Set maintainer to the pkg-mongodb project.
  * Drop disable-sslv3 patch; fixed upstream.

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 02 Sep 2016 16:28:22 +0300

mongo-tools (3.2.8-1) experimental; urgency=medium

  * Initial release (Closes: #831612)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Mon, 18 Jul 2016 12:10:57 +0300
